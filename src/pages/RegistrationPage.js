import React, { useState } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import RegistrationForm from '../components/RegistrationForm';
import api from '../axiosInstance';

function RegistrationPage() {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [message, setMessage] = useState('');

  const user = localStorage.getItem('user');
  if (user) {
    return <Navigate to="/posts" />;
  }

  async function handleFormSubmit({ username, password, fullName, file }) {
    const data = new FormData();

    data.append('username', username);
    data.append('password', password);
    data.append('fullName', fullName);
    data.append('image', file);

    try {
      setError('');
      setLoading(true);
      await api.post('registration', data, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      setMessage('Registration successfull! Redirecting to login page...');
      setTimeout(() => {
        navigate('/login');
      }, 1000);
    } catch (error) {
      setError('Ooops! Something went wrong.');
    } finally {
      setLoading(false);
    }
  }

  return (
    <div className="container h-full flex-center">
      <div>
        <h1>Register</h1>
        {message && <span className="success">{message}</span>}
        {error && <span className="error">{error}</span>}
        <RegistrationForm onSubmit={handleFormSubmit} loading={loading} />
      </div>
    </div>
  );
}

export default RegistrationPage;
