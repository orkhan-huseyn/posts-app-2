import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import api from '../axiosInstance';
import ProtectedRoute from '../components/ProtectedRoute';
import Header from '../components/Header';
import NewPostForm from '../components/NewPostForm';

function NewPostPage() {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  async function handleFormSubmit({ title, content, tags }) {
    try {
      setError('');
      setLoading(true);
      await api.post('posts', {title, content, tags });
      navigate('/posts');
    } catch (error) {
      setError('Ooops! Something went wrong.');
    } finally {
      setLoading(false);
    }
  }

  return (
    <ProtectedRoute>
      <Header />
      <div className="container">
        <Link to="/posts">⬅️ Back to posts</Link>
        <h1>Create New Post</h1>
        {error && <span className="error">{error}</span>}
        <NewPostForm onSubmit={handleFormSubmit} loading={loading} />
      </div>
    </ProtectedRoute>
  );
}

export default NewPostPage;
