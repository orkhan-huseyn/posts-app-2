import React, { useEffect, useState } from 'react';
import { Outlet, NavLink } from 'react-router-dom';
import ProtectedRoute from '../components/ProtectedRoute';
import Header from '../components/Header';
import api from '../axiosInstance';
import socket from '../socket';

function ChatsPage() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    async function getUsers() {
      const { data } = await api.get('users');
      setUsers(data);
    }

    getUsers();
    socket.connect();

    return () => {
      socket.disconnect();
    };
  }, []);

  return (
    <ProtectedRoute>
      <Header />
      <div className="chat-container">
        <div className="user-list">
          {users.map((user) => (
            <NavLink key={user.id} to={`/chat/${user.id}`} className="user">
              <div className="image">
                <img
                  src={process.env.REACT_APP_BACKEND_URL + user.image}
                  alt={user.full_name}
                />
              </div>
              <div className="user-info">
                <h4>{user.full_name}</h4>
                <small>@{user.username}</small>
                {Number(user.unread_messages) > 0 && (
                  <small className="unread">{user.unread_messages}</small>
                )}
              </div>
            </NavLink>
          ))}
        </div>
        <div className="message-list">
          <Outlet />
        </div>
      </div>
    </ProtectedRoute>
  );
}

export default ChatsPage;
