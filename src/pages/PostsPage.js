import React from 'react';
import PostList from '../components/PostList';
import LoadMore from '../components/LoadMore';
import Header from '../components/Header';
import Summary from '../components/Summary';
import ProtectedRoute from '../components/ProtectedRoute';
import { usePosts } from '../hooks/usePosts';

function PostsPage() {
  const [posts, total, loading, error, loadMore, deletePost] = usePosts();

  return (
    <ProtectedRoute>
      <Header />
      <div className="container">
        <Summary total={total} />
        <PostList
          loading={loading}
          error={error}
          posts={posts}
          deletePost={deletePost}
        />
        <LoadMore
          itemsSize={posts.length}
          total={total}
          onLoadMore={loadMore}
          loading={loading}
        />
      </div>
    </ProtectedRoute>
  );
}

export default PostsPage;
