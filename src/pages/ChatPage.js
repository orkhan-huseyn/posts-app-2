import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useParams } from 'react-router-dom';
import uniqid from 'uniqid';
import { timeAgo } from '../timeAgo';
import api from '../axiosInstance';
import socket from '../socket';

function ChatPage() {
  const { userId } = useParams();
  const [messages, setMessages] = useState([]);
  const [message, setMessage] = useState('');
  const messagesRef = useRef();

  const scrollToBottom = useCallback(() => {
    setTimeout(() => {
      const messageBox = messagesRef.current;
      messageBox.scrollTop = messageBox.scrollHeight;
    });
  }, []);

  useEffect(() => {
    socket.on('new message', ({ content, toUserId }) => {
      if (toUserId !== userId) {
        return;
      }

      setMessages((messages) => [
        ...messages,
        {
          id: uniqid(),
          content,
          is_from_self: false,
          created_at: new Date().toISOString(),
        },
      ]);
      
      scrollToBottom();
    });

    socket.emit('start chat', { userId });

    return () => {
      socket.off('new message');
    };
  }, []);

  useEffect(() => {
    async function getMessages() {
      const { data } = await api.get(`/users/${userId}/messages`);
      setMessages(data);
      scrollToBottom();
    }

    async function markAllAsRead() {
      await api.put(`/users/${userId}/messages/seen`);
    }

    getMessages();
    markAllAsRead();
  }, [userId, scrollToBottom]);

  function handleSendMessage(event) {
    event.preventDefault();

    setMessages((messages) => [
      ...messages,
      {
        id: uniqid(),
        content: message,
        is_from_self: true,
        created_at: new Date().toISOString(),
      },
    ]);

    scrollToBottom();

    socket.emit('chat message', { toUserId: userId, content: message });
    setMessage('');
  }

  return (
    <>
      <ul ref={messagesRef} className="messages">
        {messages.map((message) => (
          <li
            key={message.id}
            className={`${message.is_from_self ? 'right' : 'left'}`}
          >
            <div className="message">
              <span>{message.content}</span>
            </div>
            <small>{timeAgo.format(new Date(message.created_at))}</small>
          </li>
        ))}
      </ul>
      <form onSubmit={handleSendMessage} className="send-message-form">
        <input
          onChange={(e) => setMessage(e.target.value)}
          value={message}
          type="text"
          placeholder="Write a message to send"
        />
        <button aria-label="Send message" disabled={!message}>
          <i className="fa-solid fa-paper-plane"></i>
        </button>
      </form>
    </>
  );
}

export function EmptyChat() {
  return (
    <div className="h-chat flex-center">
      <p>Click on a user to start a chat...</p>
    </div>
  );
}

export default ChatPage;
