import React, { useState } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import api from '../axiosInstance';
import LoginForm from '../components/LoginForm';

function LoginPage() {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const user = localStorage.getItem('user');
  if (user) {
    return <Navigate to="/posts" />;
  }

  async function handleFormSubmit({ username, password }) {
    try {
      setError('');
      setLoading(true);
      const { data } = await api.post('login', { username, password });
      const userInfo = jwt_decode(data.accessToken);
      localStorage.setItem('user', JSON.stringify(userInfo));
      localStorage.setItem('access_token', data.accessToken);
      navigate('/posts');
    } catch (error) {
      setError('Ooops! Something went wrong.');
    } finally {
      setLoading(false);
    }
  }

  return (
    <div className="container h-full flex-center">
      <div>
        <h1>Log In</h1>
        {error && <span className="error">{error}</span>}
        <LoginForm onSubmit={handleFormSubmit} loading={loading} />
      </div>
    </div>
  );
}

export default LoginPage;
