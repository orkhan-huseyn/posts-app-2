import React, { useCallback, useEffect, useState } from 'react';
import { useParams, Link } from 'react-router-dom';
import api from '../axiosInstance';
import ProtectedRoute from '../components/ProtectedRoute';
import Header from '../components/Header';
import CommentForm from '../components/CommentForm';

const emptyPost = {
  title: 'Loading...',
  body: 'Loading...',
  tags: [],
  reactions: 0,
};

function PostPage() {
  const { postId } = useParams();
  const [loading, setLoading] = useState(false);
  const [post, setPost] = useState(emptyPost);
  const [comments, setComments] = useState([]);

  const getPost = useCallback(
    async function () {
      const { data } = await api.get(`posts/${postId}`);
      setPost(data);
    },
    [postId]
  );

  const getComments = useCallback(
    async function () {
      const { data } = await api.get(`posts/${postId}/comments`);
      setComments(data.comments);
    },
    [postId]
  );

  async function addComment({ content }) {
    setLoading(true);
    await api.post(`posts/${postId}/comments`, { content });
    setLoading(false);
    getComments();
  }

  useEffect(() => {
    getPost();
    getComments();
  }, [getPost, getComments]);

  return (
    <ProtectedRoute>
      <Header />
      <div className="container">
        <Link to="/posts">⬅️ Back to posts</Link>
        <h1>{post.title}</h1>
        <p>{post.body}</p>
        <p className="flex-between">
          <span>
            {post.tags.map((tag) => (
              <span key={tag} className="tag">
                {tag}
              </span>
            ))}
          </span>
          <span>❤️ {post.reactions}</span>
        </p>

        <h2>Comments </h2>

        <div className="comment-list">
          {comments.map((comment) => (
            <div key={comment.id} className="comment">
              <span className="user">@{comment.username}</span>
              <p>{comment.body}</p>
            </div>
          ))}
        </div>

        <CommentForm onSubmit={addComment} loading={loading} />
      </div>
    </ProtectedRoute>
  );
}

export default PostPage;
