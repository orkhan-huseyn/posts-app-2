import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import LoginPage from './pages/LoginPage';
import PostsPage from './pages/PostsPage';
import PostPage from './pages/PostPage';
import RegistrationPage from './pages/RegistrationPage';
import NewPostPage from './pages/NewPostPage';
import ChatsPage from './pages/ChatsPage';
import ChatPage, { EmptyChat } from './pages/ChatPage';

function App() {
  return (
    <Routes>
      <Route path="/" element={<Navigate to="/login" />} />
      <Route path="/login" element={<LoginPage />} />
      <Route path="/registration" element={<RegistrationPage />} />
      <Route path="/posts" element={<PostsPage />} />
      <Route path="/posts/new" element={<NewPostPage />} />
      <Route path="/posts/:postId" element={<PostPage />} />
      <Route path="/chat" element={<ChatsPage />}>
        <Route path="" element={<EmptyChat />} />
        <Route path=":userId" element={<ChatPage />} />
      </Route>
    </Routes>
  );
}

export default App;
