import { io } from 'socket.io-client';

const socket = io(process.env.REACT_APP_SOCKET_URL, {
  autoConnect: false,
  reconnection: true,
  query: {
    accessToken: localStorage.getItem('access_token'),
  },
});

export default socket;
