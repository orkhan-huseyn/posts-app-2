import api from '../axiosInstance';
import { useCallback, useEffect, useState } from 'react';

const LIMIT = 5;

export function usePosts() {
  const [skip, setSkip] = useState(0);
  const [total, setTotal] = useState(0);
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const getPosts = useCallback(
    async function () {
      try {
        const params = { limit: LIMIT, skip };
        setLoading(true);
        const { data } = await api.get('posts', {
          params,
        });
        setPosts(function (oldPosts) {
          return [...oldPosts, ...data.posts];
        });
        setTotal(data.total);
      } catch (error) {
        setError('Something went wrong!');
      } finally {
        setLoading(false);
      }
    },
    [skip]
  );

  useEffect(() => {
    getPosts();
  }, [skip, getPosts]);

  function deletePost(id) {
    setPosts(posts.filter((post) => post.id !== id));
  }

  function loadMore() {
    setSkip(function (oldSkip) {
      return oldSkip + LIMIT;
    });
  }

  return [posts, total, loading, error, loadMore, deletePost];
}
