import React, { useState } from 'react';

const MAX_ALLOWED_TAGS = 5;

function NewPostForm({ onSubmit, loading = false }) {
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');
  const [tag, setTag] = useState('');
  const [tags, setTags] = useState([]);

  function handleFormSubmit(e) {
    e.preventDefault();
    onSubmit({ title, content: body, tags });
  }

  function handleKeyDown(e) {
    if (e.key === 'Enter') {
      e.preventDefault();
      tags.push(tag);
      setTag('');
    }
  }

  function removeTag(index) {
    const tagsCopy = tags.slice();
    tagsCopy.splice(index, 1);
    setTags(tagsCopy);
  }

  return (
    <form onSubmit={handleFormSubmit} className="app-form w-full">
      <div className="group">
        <label htmlFor="title">Title:</label>
        <input
          id="title"
          placeholder="Enter title..."
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
      </div>
      <div className="group">
        <label htmlFor="body">Body:</label>
        <textarea
          id="body"
          value={body}
          rows={10}
          onChange={(e) => setBody(e.target.value)}
        ></textarea>
      </div>
      <div className="group">
        <label htmlFor="tags">Tags (up to {MAX_ALLOWED_TAGS} tags):</label>
        <input
          id="tags"
          placeholder="Enter tag..."
          value={tag}
          onChange={(e) => setTag(e.target.value)}
          onKeyDown={handleKeyDown}
          disabled={tags.length === MAX_ALLOWED_TAGS}
        />
      </div>
      <div className="group">
        {tags.map((tag, index) => (
          <span
            key={index}
            className="tag pointer"
            tabIndex={0}
            onKeyDown={(e) => e.key === 'Enter' && removeTag(index)}
            onClick={() => removeTag(index)}
          >
            {tag}
          </span>
        ))}
      </div>
      <button disabled={!title || !body || loading}>
        {loading ? 'Loading...' : 'Create Post'}
      </button>
    </form>
  );
}

export default NewPostForm;
