import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { timeAgo } from '../timeAgo';
import api from '../axiosInstance';

function PostItem({ post, deletePost }) {
  const user = JSON.parse(localStorage.getItem('user'));

  const [deleting, setDeleting] = useState(false);
  const [liked, setLiked] = useState(false);
  const [count, setCount] = useState(+post.reactions || 0);

  async function reactToPost(postId) {
    await api.post(`posts/${postId}/reactions`);
    if (liked) {
      setCount((count) => count - 1);
    } else {
      setCount((count) => count + 1);
    }
    setLiked((liked) => !liked);
  }

  async function requestDeletePost(postId) {
    setDeleting(true);
    await api.delete(`posts/${postId}`);
    setDeleting(false);
    deletePost(postId);
  }

  useEffect(() => {
    const fetchReactions = async () => {
      const { data } = await api.get(`posts/${post.id}/reactions`);
      const myReaction = data.find((reaction) => reaction.user_id === user.id);
      if (myReaction) {
        setLiked(true);
      }
    };
    fetchReactions();
  }, [post.id, user.id]);

  return (
    <div className="post">
      <div className="header">
        <Link to={`/posts/${post.id}`}>
          <h4>{post.title}</h4>
        </Link>
        {post.username === user.username && (
          <button
            disabled={deleting}
            onClick={() => requestDeletePost(post.id)}
          >
            {deleting ? 'Deleting...' : 'Delete'}
          </button>
        )}
      </div>
      <p>{post.body}</p>
      <p className="footer">
        <span>
          {post.tags.map((tag) => (
            <span key={tag} className="tag">
              {tag}
            </span>
          ))}
        </span>
      </p>
      <div className="meta">
        <b>@{post.username}</b>
        <small>{timeAgo.format(new Date(post.created_at))}</small>
        <div>
          <small>{post.comments} comments</small>
          <button onClick={() => reactToPost(post.id)}>
            {liked ? '❤️' : '💔'} {count}
          </button>
        </div>
      </div>
    </div>
  );
}

export default PostItem;
