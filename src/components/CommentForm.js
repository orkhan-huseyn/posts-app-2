import React, { useState } from 'react';

function CommentForm({ onSubmit, loading = false }) {
  const [body, setBody] = useState('');

  function handleFormSubmit(e) {
    e.preventDefault();
    onSubmit({ content: body });
    setBody('');
  }

  return (
    <form onSubmit={handleFormSubmit} className="app-form w-full">
      <div className="group">
        <textarea
          value={body}
          rows={5}
          onChange={(e) => setBody(e.target.value)}
          aria-label="Comment"
          placeholder="What are your thoughts about this post?"
        ></textarea>
      </div>
      <button disabled={!body || loading}>
        {loading ? 'Loading...' : 'Comment'}
      </button>
    </form>
  );
}

export default CommentForm;
