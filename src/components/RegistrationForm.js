import React, { useState } from 'react';
import { Link } from 'react-router-dom';

function RegistrationForm({ onSubmit, loading = false }) {
  const [fullName, setFullName] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [file, setFile] = useState(null);

  function handleFormSubmit(e) {
    e.preventDefault();
    onSubmit({ fullName, username, password, file });
  }

  return (
    <form onSubmit={handleFormSubmit} className="app-form">
      <div className="group">
        <label htmlFor="fullName">Full Name:</label>
        <input
          id="fullName"
          placeholder="Enter full name..."
          value={fullName}
          onChange={(e) => setFullName(e.target.value)}
        />
      </div>
      <div className="group">
        <label htmlFor="username">Username:</label>
        <input
          id="username"
          placeholder="Enter username..."
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
      </div>
      <div className="group">
        <label htmlFor="password">Password:</label>
        <input
          type="password"
          id="password"
          placeholder="Enter password..."
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>
      <div className="group">
        <label htmlFor="profilePhoto">Profile Photo:</label>
        <input
          type="file"
          id="profilePhoto"
          accept="image/png, image/gif, image/jpeg"
          onChange={(e) => setFile(e.target.files[0])}
        />
      </div>
      <p>
        Already have an account? <Link to="/login">Sign in</Link>
      </p>
      <button disabled={!username || !password || loading}>
        {loading ? 'Loading...' : 'Sign Up'}
      </button>
    </form>
  );
}

export default RegistrationForm;
