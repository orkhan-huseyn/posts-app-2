import React, { useState } from 'react';
import { Link } from 'react-router-dom';

function LoginForm({ onSubmit, loading = false }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  function handleFormSubmit(e) {
    e.preventDefault();
    onSubmit({ username, password });
  }

  return (
      <form onSubmit={handleFormSubmit} className="app-form">
        <div className="group">
          <label htmlFor="username">Username:</label>
          <input
            id="username"
            placeholder="Enter username..."
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className="group">
          <label htmlFor="password">Password:</label>
          <input
            type="password"
            id="password"
            placeholder="Enter password..."
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <p>
          Don't have an account? <Link to="/registration">Sign up</Link>
        </p>
        <button disabled={!username || !password || loading}>
          { loading ? 'Loading...' : 'Sign In' }
        </button>
      </form>
  );
}

export default LoginForm;
