import React from 'react';
import PostItem from './PostItem';

function PostList({ loading, error, posts, deletePost }) {
  return (
    <div className="post-list">
      {loading && <span className="loading">Loading...</span>}
      {error && <span className="error">{error}</span>}
      {posts.map(post => (
          <PostItem key={post.id} deletePost={deletePost} post={post}/>
      ))}
    </div>
  );
}

export default PostList;
