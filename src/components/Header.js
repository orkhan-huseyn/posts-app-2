import React from 'react';
import { Link, useNavigate } from 'react-router-dom';

function Header() {
  const navigate = useNavigate();
  const user = JSON.parse(localStorage.getItem('user'));

  function handleLogout() {
    localStorage.removeItem('user');
    localStorage.removeItem('access_token');
    navigate('/login');
  }

  return (
    <header className="header">
      <div className="user">
        <img src={process.env.REACT_APP_BACKEND_URL + user.image} alt={user.fullName} />
        <span>{user.username}</span>
      </div>
      <Link to="/chat" className="button button-secondary">
        <i className="fa-solid fa-message"></i> Chat
      </Link>
      <Link to="/posts/new" className="button button-action">
        <i className="fa-solid fa-plus"></i> New Post
      </Link>
      <button onClick={handleLogout} className="button">
        <i className="fa-solid fa-arrow-right-from-bracket"></i> Log Out
      </button>
    </header>
  );
}

export default Header;
