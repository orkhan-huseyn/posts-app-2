# Functional programming

immutability
pure functions - no side effect
higher order functions

side effect -
1. randomness
2. API calls
3. setTimeout, setInterval
4. mutating global state
5. mutating function arguments
